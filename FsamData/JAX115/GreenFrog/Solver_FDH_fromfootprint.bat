hippo_habitat_client ^
--run_nap_packing "true" ^
--run_fdh_packing "true" ^
--run_distribution_solver "false" ^
--mip_stop_gap_nap "0.001" ^
--mip_stop_gap_fdh "0.01" ^
--mip_stop_gap_dist "0.01" ^
--access_cabling_algo "detailed_solve_algo" ^
--reuse_factor "0.80" ^
--intersection-crossing-multiplier "1" ^
--crossing-multiplier "10" ^
--candidate_graph    "output_preprocessor\candidate_graph.json" ^
--graph_to_geom      "output_preprocessor\graph_to_geom.json" ^
--hut_node_graph     "output_preprocessor\hut_node_graph.json" ^
--demand_node_graph  "output_preprocessor\demand_node_graph.json" ^
--drop-cable-blocker   "cable_blocker\drop_blocker_115.shp" ^
--mst-cable-blocker    "cable_blocker\mst_blocker_115.shp" ^
--nap-cable-blocker    "cable_blocker\access_blocker_bound_115.shp" ^
--cache ".\output_solver\fap\design_graph.json" ^
--mst-footprint    "candidate_positions\footprint_mst.shp" ^
--access-footprint "candidate_positions\footprint_access.shp" ^
--fdh_geometry     "candidate_positions\hubs.shp" ^
--logfile ".\output_solver\fdh_fromfootprint\log.log" ^
--output_dir ".\output_solver\fdh_fromfootprint" ^
--server_url "https://greenfrog.biarrinetworks.com"

::--fdh_geometry     "candidate_positions\hubs.shp" ^

::--cache "output_solver\mst_foot\design_graph.json" ^

::



::--treat_input_hubs_as "preference_positions" ^
::--discount_preference_hubs 0.1 ^
::--mst_geometry "roadside_preferencing\roadside.shp" ^ (preferenced candidate nodes)
::--penalty_edges "roadside_preferencing\penalty_edges.shp" ^ (unpreferenced roadside edges)
::--penalty_factor 5 ^
::--crossing-multiplier -0.5 ^