hippo_habitat_client ^
--run_nap_packing "false" ^
--run_fdh_packing "false" ^
--run_distribution_solver "false" ^
--mip_stop_gap_nap "0.001" ^
--mip_stop_gap_fdh "0.005" ^
--mip_stop_gap_dist "0.01" ^
--access_cabling_algo "detailed_solve_algo" ^
--reuse_factor "0.80" ^
--intersection-crossing-multiplier "1" ^
--crossing-multiplier "10" ^
--treat_input_hubs_as "fixed_positions" ^
--drop-footprint   "candidate_positions\footprint_drop.shp" ^
--mst_geometry     "candidate_positions\msts.shp" ^
--mst-footprint    "candidate_positions\footprint_mst.shp" ^
--fap_geometry     "candidate_positions\naps.shp" ^
--candidate_graph    "output_preprocessor\candidate_graph.json" ^
--graph_to_geom      "output_preprocessor\graph_to_geom.json" ^
--hut_node_graph     "output_preprocessor\hut_node_graph.json" ^
--demand_node_graph  "output_preprocessor\demand_node_graph.json" ^
--drop-cable-blocker   "cable_blocker\drop_blocker_115.shp" ^
--mst-cable-blocker    "cable_blocker\mst_blocker_115.shp" ^
--logfile ".\output_solver\mst\log.log" ^
--output_dir ".\output_solver\mst" ^
--server_url "https://greenfrog.biarrinetworks.com"

::--cache "output_solver\FOOTPRINT_fdh_detailed\design_graph.json" ^

::--cache "output_solver\mst_foot\design_graph.json" ^

::--access-footprint "candidate_positions\footprint_access.shp" ^
::--fdh_geometry     "candidate_positions\hubs.shp" ^



::--treat_input_hubs_as "preference_positions" ^
::--discount_preference_hubs 0.1 ^
::--mst_geometry "roadside_preferencing\roadside.shp" ^ (preferenced candidate nodes)
::--penalty_edges "roadside_preferencing\penalty_edges.shp" ^ (unpreferenced roadside edges)
::--penalty_factor 5 ^
::--crossing-multiplier -0.5 ^