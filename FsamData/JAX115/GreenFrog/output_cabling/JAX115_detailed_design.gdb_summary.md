# Layer: `GID_CAD`
### AssetID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GIDDescription
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### CAD
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### CADDescription
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GPN
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Optional
* Type: `STRING`
* Width: `5`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Quantity
* Type: `REAL`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `HUTAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### HUTID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### EngGuidelines
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ConstTypicals
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `MSTAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### MSTID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `POLE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `Paul Sulisz`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 6
* Number of features: 14803
* Empty values: 0
* Random value: ``

### OwnerPoleID
* Type: `STRING`
* Width: `50`
* Unique values: 13306
* Number of features: 14803
* Empty values: 0
* Random value: `POL-1048064`

### AlternatePoleID
* Type: `STRING`
* Width: `50`
* Unique values: 13336
* Number of features: 14803
* Empty values: 0
* Random value: `OH-1109958`

### Use
* Type: `STRING`
* Width: `50`
* Unique values: 9
* Number of features: 14803
* Empty values: 0
* Random value: ``

### Height
* Type: `REAL`
* No 'width' specified.
* Unique values: 21
* Number of features: 14803
* Empty values: 0
* Random value: `45`

### Material
* Type: `STRING`
* Width: `50`
* Unique values: 9
* Number of features: 14803
* Empty values: 0
* Random value: `DC`

### Attachable
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `ATTACH`

### AttachmentCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 27
* Number of features: 14803
* Empty values: 0
* Random value: `9`

### AttachmentHeight1
* Type: `REAL`
* No 'width' specified.
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `0`

### AttachmentHeight2
* Type: `REAL`
* No 'width' specified.
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `0`

### AttachmentHeight3
* Type: `REAL`
* No 'width' specified.
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `0`

### AttachmentSide
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### StandInstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `0`

### ClampType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### DeadendType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `Single`

### Grounding
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 14803
* Empty values: 0
* Random value: ``

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 4
* Number of features: 14803
* Empty values: 0
* Random value: `MST`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 14803
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 4
* Number of features: 14803
* Empty values: 0
* Random value: `None`

# Layer: `PERMIT_BUILD`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `FDHAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FDHID
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DemandSegment
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `GUY_SPAN`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `Paul Sulisz`

### ToStructure
* Type: `STRING`
* Width: `50`
* Unique values: 2436
* Number of features: 2617
* Empty values: 0
* Random value: `POL-1023762`

### FromStructure
* Type: `STRING`
* Width: `50`
* Unique values: 2436
* Number of features: 2617
* Empty values: 0
* Random value: `A1-POL-1004660`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `0`

### GuyType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `Down Guy`

### StrandSize
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `PSR`

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

# Layer: `FDHPOINT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: `Paul Sulisz`

### FDHID
* Type: `STRING`
* Width: `100`
* Unique values: 79
* Number of features: 79
* Empty values: 0
* Random value: `JAX115l-F004`

### LocationDescription
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: ``

### AddressID
* Type: `STRING`
* Width: `255`
* Unique values: 78
* Number of features: 79
* Empty values: 0
* Random value: `de6c353d9ae22f8285e63ae623d8077f`

### DFCableName
* Type: `STRING`
* Width: `150`
* Unique values: 15
* Number of features: 79
* Empty values: 0
* Random value: `JAX115l`

### AFCableName
* Type: `STRING`
* Width: `150`
* Unique values: 79
* Number of features: 79
* Empty values: 0
* Random value: `JAX115e-F002a,JAX115e-F002b,JAX115e-F002c`

### Number1x4
* Type: `INTEGER`
* Width: `150`
* Unique values: 16
* Number of features: 79
* Empty values: 0
* Random value: `17`

### Number1x8
* Type: `INTEGER`
* Width: `150`
* Unique values: 7
* Number of features: 79
* Empty values: 0
* Random value: `5`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: `PSR`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 79
* Empty values: 0
* Random value: `Biarri`

# Layer: `HUTPOINT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `Paul Sulisz`

### HUTID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `JAX115`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### LocationDescription
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### AddressID
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `e04ffb7cccd8d13bfa11948cae6a44c2`

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `PSR`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `Biarri`

# Layer: `PERMITAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### PermitType
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### CityPermitDescription
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Agency
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ImpactedScope
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### PermitDuration
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateIdentified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateApplied
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateApproved
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateClosed
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `FAPAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FAPID
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FAPType
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `CONDUIT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `Paul Sulisz`

### CalculatedLength
* Type: `REAL`
* Width: `4`
* Unique values: 1821
* Number of features: 2343
* Empty values: 0
* Random value: `43.4829`

### MeasuredLength
* Type: `REAL`
* Width: `4`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `0`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `0`

### Diameter
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 2343
* Empty values: 0
* Random value: `0.5inch`

### Category
* Type: `STRING`
* Width: `100`
* Unique values: 4
* Number of features: 2343
* Empty values: 0
* Random value: `AF`

### ToStructure
* Type: `STRING`
* Width: `50`
* Unique values: 854
* Number of features: 2343
* Empty values: 0
* Random value: `GLS-JAX115j-F001c-0010`

### FromStructure
* Type: `STRING`
* Width: `50`
* Unique values: 989
* Number of features: 2343
* Empty values: 0
* Random value: `GLS-JAX115e-F003c,097-0002`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `PSR`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `Google`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 2343
* Empty values: 0
* Random value: `AF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `500`
* Unique values: 1807
* Number of features: 2343
* Empty values: 0
* Random value: `JAX115a-F002b,078`

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 2343
* Empty values: 0
* Random value: `Biarri`

### CableCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 16
* Number of features: 2343
* Empty values: 0
* Random value: `6`

# Layer: `SLACKLOOP`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: `Paul Sulisz`

### CableName
* Type: `STRING`
* Width: `350`
* Unique values: 209
* Number of features: 524
* Empty values: 0
* Random value: `JAX115b-F003a,032`

### CableCapacity
* Type: `STRING`
* Width: `10`
* Unique values: 4
* Number of features: 524
* Empty values: 0
* Random value: `432`

### MeasuredLength
* Type: `REAL`
* Width: `4`
* Unique values: 3
* Number of features: 524
* Empty values: 0
* Random value: `150`

### Placement
* Type: `STRING`
* Width: `100`
* Unique values: 4
* Number of features: 524
* Empty values: 0
* Random value: `Underground`

### SegmentID
* Type: `STRING`
* Width: `90`
* Unique values: 9
* Number of features: 524
* Empty values: 0
* Random value: `6`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 3
* Number of features: 524
* Empty values: 0
* Random value: `AF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 524
* Empty values: 0
* Random value: `Biarri`

# Layer: `BUILDINGOUTLINE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### BuildingName
* Type: `STRING`
* Width: `150`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Address
* Type: `STRING`
* Width: `150`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### City
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### State
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ZipCode
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### UnitCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ContractStatus
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### PropertyID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LandlordID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FiberEquipmentID
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `ADDRESS`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: `Paul Sulisz`

### AddressID
* Type: `STRING`
* Width: `250`
* Unique values: 37029
* Number of features: 38916
* Empty values: 0
* Random value: `67284b0103d1be3ae31cf0ebffb1ca72`

### AddressType
* Type: `STRING`
* Width: `250`
* Unique values: 15
* Number of features: 38916
* Empty values: 0
* Random value: `LARGE_COMMERCIAL_UNIT`

### StreetAddress
* Type: `STRING`
* Width: `250`
* Unique values: 28127
* Number of features: 38916
* Empty values: 0
* Random value: `6822 LAKE MIST LN`

### UnitNumber
* Type: `STRING`
* Width: `250`
* Unique values: 2719
* Number of features: 38916
* Empty values: 0
* Random value: `APT 8205`

### Zipcode
* Type: `STRING`
* Width: `250`
* Unique values: 7172
* Number of features: 38916
* Empty values: 0
* Random value: `32254-4208`

### City
* Type: `STRING`
* Width: `250`
* Unique values: 7
* Number of features: 38916
* Empty values: 0
* Random value: `JAKESONVILLE`

### State
* Type: `STRING`
* Width: `250`
* Unique values: 3
* Number of features: 38916
* Empty values: 0
* Random value: `FL`

### Latitude
* Type: `STRING`
* Width: `250`
* Unique values: 2
* Number of features: 38916
* Empty values: 0
* Random value: ``

### Longitude
* Type: `STRING`
* Width: `250`
* Unique values: 2
* Number of features: 38916
* Empty values: 0
* Random value: ``

### DataSource
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: ``

### CoordinatesAccuracy
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: `UNKNOWN_COORDINATE_ACCURACY`

### OnPrivateRoad
* Type: `STRING`
* Width: `5`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: ``

### ProjectID
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: `PSR`

### DateAcceptedForDesign
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: `0`

### DropType
* Type: `STRING`
* Width: `50`
* Unique values: 5
* Number of features: 38916
* Empty values: 0
* Random value: `UGToAerial`

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 38916
* Empty values: 0
* Random value: ``

# Layer: `PATH`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `Paul Sulisz`

### Placement
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13472
* Empty values: 0
* Random value: `Underground`

### StrandSize
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13472
* Empty values: 0
* Random value: ``

### SquirrelGuard
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: ``

### TreeGuard
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: ``

### SurfaceType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `None`

### InstallMethod
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `None`

### Density
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `RS`

### ToneWire
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13472
* Empty values: 0
* Random value: `N`

### Easement
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13472
* Empty values: 0
* Random value: ``

### CalculatedLength
* Type: `REAL`
* Width: `4`
* Unique values: 13328
* Number of features: 13472
* Empty values: 0
* Random value: `275.4918`

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: `PSR`

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 13472
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13472
* Empty values: 0
* Random value: ``

### ConduitDiameterSum
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 13472
* Empty values: 0
* Random value: `14`

### CableCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 21
* Number of features: 13472
* Empty values: 0
* Random value: `14`

# Layer: `RISER`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `Paul Sulisz`

### RiserID
* Type: `STRING`
* Width: `50`
* Unique values: 226
* Number of features: 241
* Empty values: 0
* Random value: `R1-POL-1004918`

### PoleID
* Type: `STRING`
* Width: `50`
* Unique values: 226
* Number of features: 241
* Empty values: 0
* Random value: `POL-1041151`

### CableName
* Type: `STRING`
* Width: `350`
* Unique values: 215
* Number of features: 241
* Empty values: 0
* Random value: `JAX115l-F004c`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `None`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

### RiserCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `2`

### Diameter
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: `2`

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 241
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 241
* Empty values: 0
* Random value: ``

# Layer: `STRUCTURE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `Paul Sulisz`

### StructureName
* Type: `STRING`
* Width: `50`
* Unique values: 1378
* Number of features: 1378
* Empty values: 0
* Random value: `GLS-JAX115l-F003a-0001`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `0`

### StructureForm
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `Vault`

### PrimaryUse
* Type: `STRING`
* Width: `50`
* Unique values: 7
* Number of features: 1378
* Empty values: 0
* Random value: `Pull`

### VaultSize
* Type: `STRING`
* Width: `50`
* Unique values: 5
* Number of features: 1378
* Empty values: 0
* Random value: `LV`

### LidType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `Solid`

### Rating
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `Tier15`

### LidMaterial
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `HDPE`

### LidRing
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `N`

### Grounded
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 1378
* Empty values: 0
* Random value: `Y`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `PSR`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `Google`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 3
* Number of features: 1378
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1378
* Empty values: 0
* Random value: `Biarri`

# Layer: `FIBEREQUIPMENT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `Paul Sulisz`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `0`

### EquipmentForm
* Type: `STRING`
* Width: `25`
* Unique values: 3
* Number of features: 32852
* Empty values: 0
* Random value: `Splitter`

### EquipmentName
* Type: `STRING`
* Width: `250`
* Unique values: 32852
* Number of features: 32852
* Empty values: 0
* Random value: `JAX115m-F005a-017_MST`

### Placement
* Type: `STRING`
* Width: `100`
* Unique values: 3
* Number of features: 32852
* Empty values: 0
* Random value: `Aerial`

### SplitRatio
* Type: `STRING`
* Width: `25`
* Unique values: 3
* Number of features: 32852
* Empty values: 0
* Random value: `SPLT_4`

### InputPortCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `1`

### InputPortAssigned
* Type: `STRING`
* Width: `50`
* Unique values: 22
* Number of features: 32852
* Empty values: 0
* Random value: `16`

### OutputPortCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 32852
* Empty values: 0
* Random value: `3`

### OutputPortsAssigned
* Type: `STRING`
* Width: `50`
* Unique values: 937
* Number of features: 32852
* Empty values: 0
* Random value: `7-8,16,19`

### InputBlockCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `1`

### OutputBlockCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `1`

### InputStrandsAssigned
* Type: `STRING`
* Width: `250`
* Unique values: 32456
* Number of features: 32852
* Empty values: 0
* Random value: `JAX115e-F005c,103-3(1)`

### OutputStrandsAssigned
* Type: `STRING`
* Width: `250`
* Unique values: 6179
* Number of features: 32852
* Empty values: 0
* Random value: `JAX115d-F005a,030-1(1);JAX115d-F005a,030-2(1);JAX115d-F005a,030-3(1);JAX115d-F005a,030-4(1);JAX115d-F005a,030-5(1);JAX115d-F005a,030-6(1)`

### StrandsAssigned
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 32852
* Empty values: 0
* Random value: `8`

### CableCapacity
* Type: `STRING`
* Width: `10`
* Unique values: 2
* Number of features: 32852
* Empty values: 0
* Random value: `1`

### OutputPortID
* Type: `STRING`
* Width: `50`
* Unique values: 4933
* Number of features: 32852
* Empty values: 0
* Random value: `JAX115m-F001c(115)`

### AddressID
* Type: `STRING`
* Width: `250`
* Unique values: 25559
* Number of features: 32852
* Empty values: 0
* Random value: `9b9dc7d0919c426d4679199d2999d580`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 32852
* Empty values: 0
* Random value: `MST`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### CircuitID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 32852
* Empty values: 0
* Random value: `Biarri`

# Layer: `WIFI`
### WiFiID
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Latitude
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Longitude
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Comments
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `SPLICECLOSURE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: `Paul Sulisz`

### SpliceID
* Type: `STRING`
* Width: `255`
* Unique values: 1020
* Number of features: 1020
* Empty values: 0
* Random value: `JAX115c-F005b-AFF2`

### StructureName
* Type: `STRING`
* Width: `184`
* Unique values: 236
* Number of features: 1020
* Empty values: 0
* Random value: `GLS-JAX115e-F006a-0021`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: `0`

### FiberAssignments
* Type: `STRING`
* Width: `150`
* Unique values: 997
* Number of features: 1020
* Empty values: 0
* Random value: `JAX115d(1-9) + SP(10-72) + XD(73-144)`

### SpliceType
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 1020
* Empty values: 0
* Random value: `SINGLE`

### Placement
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 1020
* Empty values: 0
* Random value: `Aerial`

### ClosureUse
* Type: `STRING`
* Width: `50`
* Unique values: 4
* Number of features: 1020
* Empty values: 0
* Random value: `FDHSplice`

### ClosureSize
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 1020
* Empty values: 0
* Random value: `C`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 1020
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: ``

### RibbonizedSplices
* Type: `INTEGER`
* Width: `100`
* Unique values: 3
* Number of features: 1020
* Empty values: 0
* Random value: `3`

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1020
* Empty values: 0
* Random value: `Biarri`

# Layer: `ANCHOR`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `Paul Sulisz`

### AnchorID
* Type: `STRING`
* Width: `50`
* Unique values: 2436
* Number of features: 2617
* Empty values: 0
* Random value: `A1-POL-1123912`

### PoleID
* Type: `STRING`
* Width: `50`
* Unique values: 2436
* Number of features: 2617
* Empty values: 0
* Random value: `POL-1011821`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `0`

### Direction
* Type: `REAL`
* No 'width' specified.
* Unique values: 2617
* Number of features: 2617
* Empty values: 0
* Random value: `307.142614968`

### AnchorType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `AC00`

### StrandSize
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `1_4`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `PSR`

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `None`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 2617
* Empty values: 0
* Random value: `Biarri`

# Layer: `FIBERCABLE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `2016-06-23`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `2016-06-23`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `Paul Sulisz`

### Cablename
* Type: `STRING`
* Width: `500`
* Unique values: 33132
* Number of features: 34542
* Empty values: 0
* Random value: `JAX115c-F003b,067`

### Leg
* Type: `STRING`
* Width: `20`
* Unique values: 16
* Number of features: 34542
* Empty values: 0
* Random value: `c`

### Category
* Type: `STRING`
* Width: `100`
* Unique values: 4
* Number of features: 34542
* Empty values: 0
* Random value: `DF`

### BufferCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 5
* Number of features: 34542
* Empty values: 0
* Random value: `18`

### FibersPerBuffer
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 2
* Number of features: 34542
* Empty values: 0
* Random value: `12`

### BatchNumber_ReelNumber
* Type: `STRING`
* Width: `20`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### SerialNumber
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### ToFeature
* Type: `STRING`
* Width: `50`
* Unique values: 32706
* Number of features: 34542
* Empty values: 0
* Random value: `JAX115c-F006b,049-8_NIU`

### FromFeature
* Type: `STRING`
* Width: `50`
* Unique values: 5946
* Number of features: 34542
* Empty values: 0
* Random value: `JAX115a-F003a-027_MST`

### CableCapacity
* Type: `STRING`
* Width: `10`
* Unique values: 5
* Number of features: 34542
* Empty values: 0
* Random value: `432`

### AssignedFiberCount
* Type: `STRING`
* Width: `250`
* Unique values: 33878
* Number of features: 34542
* Empty values: 0
* Random value: `JAX115l-F005a,006(1)`

### AssignedFibers
* Type: `STRING`
* Width: `50`
* Unique values: 202
* Number of features: 34542
* Empty values: 0
* Random value: `1-22,37-48`

### DeadFibers
* Type: `STRING`
* Width: `50`
* Unique values: 41
* Number of features: 34542
* Empty values: 0
* Random value: ``

### SpareFibers
* Type: `STRING`
* Width: `50`
* Unique values: 196
* Number of features: 34542
* Empty values: 0
* Random value: `90-144`

### IsOddCountFront
* Type: `STRING`
* Width: `5`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `N`

### FirstFiber
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 12
* Number of features: 34542
* Empty values: 0
* Random value: `49`

### Placement
* Type: `STRING`
* Width: `100`
* Unique values: 4
* Number of features: 34542
* Empty values: 0
* Random value: `UGToAerial`

### Core
* Type: `STRING`
* Width: `112`
* Unique values: 2
* Number of features: 34542
* Empty values: 0
* Random value: `RIBBON`

### CalculatedLength
* Type: `REAL`
* Width: `4`
* Unique values: 32403
* Number of features: 34542
* Empty values: 0
* Random value: `93.1486`

### MeasuredLength
* Type: `REAL`
* Width: `4`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `0`

### StubLength
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 11
* Number of features: 34542
* Empty values: 0
* Random value: `1000`

### DropLength
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 34542
* Empty values: 0
* Random value: `500`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `0`

### SequenceIN
* Type: `STRING`
* Width: `20`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### SequenceOut
* Type: `STRING`
* Width: `20`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### SegmentID
* Type: `STRING`
* Width: `90`
* Unique values: 53
* Number of features: 34542
* Empty values: 0
* Random value: `24`

### CircuitID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### ConduitIPID
* Type: `STRING`
* Width: `350`
* Unique values: 1214
* Number of features: 34542
* Empty values: 0
* Random value: `JAX115j-F001c,114,JAX115j-F001c,115,JAX115j-F001c,113;JAX115j-F001c,114,JAX115j-F001c,115;JAX115j-F001c,114,JAX115j-F001c,115,JAX115j-F001c,113;JAX115j-F001c,114,JAX115j-F001c,115,JAX115j-F001c,112,JAX115j-F001c,113,JAX115j-F001c,111;JAX115j-F001c,114;JAX115j-F001c,114,JAX115j-F001c,110,JAX115j-F001c,115,JAX115j-F001c,112,JAX115j-F001c,113,JAX115j-`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `PSR`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `Google`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 34542
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 34542
* Empty values: 0
* Random value: `Biarri`

